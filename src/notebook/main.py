from src.notebook.classes.user_interface import UserInterface


def main(path=None):
    if path:
        ui = UserInterface(path=path)
    else:
        ui = UserInterface()
    ui.loop()


if __name__ == '__main__':
    main()
