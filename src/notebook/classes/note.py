import re

from src.notebook.classes.exceptions.no_input_firstname_exception import NoInputFirstnameException
from src.notebook.classes.exceptions.no_input_lastname_exception import NoInputLastnameException
from src.notebook.classes.exceptions.no_input_phone_exception import NoInputPhoneException
from src.notebook.classes.exceptions.wrong_first_name_exception import WrongFirstNameException
from src.notebook.classes.exceptions.wrong_last_name_exception import WrongLastNameException
from src.notebook.classes.exceptions.wrong_phone_exception import WrongPhoneException


class Note:

    def __init__(self, first_name, last_name, phone_number, address, birth_date, note_id=None):
        self.first_name = first_name
        self.last_name = last_name
        self.phone_number = phone_number
        self.address = address
        self.birth_date = birth_date
        self.id = note_id
        self.check_note()

    @staticmethod
    def check_phone_number(phone_number):
        pattern = r'\+\d{12}\Z'
        regex = re.compile(pattern)
        mo = regex.search(phone_number)
        return bool(mo)

    @staticmethod
    def check_name(name):
        pattern = r'\A\D+\Z'
        regex = re.compile(pattern)
        mo = regex.search(name)
        return bool(mo)

    def check_note(self):
        bool(self.first_name)
        if not self.first_name:
            raise NoInputFirstnameException
        if not self.last_name:
            raise NoInputLastnameException
        if not self.phone_number:
            raise NoInputPhoneException
        if not self.check_phone_number(self.phone_number):
            raise WrongPhoneException
        if not self.check_name(self.first_name):
            raise WrongFirstNameException
        if not self.check_name(self.last_name):
            raise WrongLastNameException

    def modify_note(
            self,
            first_name=None,
            last_name=None,
            phone_number=None,
            address=None,
            birth_date=None,
            note_id=None):
        if first_name is not None:
            self.first_name = first_name
        if last_name is not None:
            self.last_name = last_name
        if phone_number is not None:
            self.phone_number = phone_number
        if address is not None:
            self.address = address
        if birth_date is not None:
            self.birth_date = birth_date
        if note_id is not None:
            self.id = note_id
        self.check_note()

