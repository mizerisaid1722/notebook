import re
from tabulate import tabulate


class Notebook:

    def __init__(self):
        self.__notes = []

    def __iter__(self):
        for note in self.__notes:
            yield note

    def get_next_note_id(self):
        if self.__notes:
            return max([note.id for note in self.__notes]) + 1
        return 1

    def __len__(self):
        return len(self.__notes)

    def append(self, note):
        note.id = self.get_next_note_id()
        self.__notes.append(note)

    def delete_note(self, note):
        self.__notes.remove(note)

    def show_notes(
            self,
            first_name_filter: str = None,
            last_name_filter: str = None,
            phone_number_filter: str = None,
            first_name_sort: bool = False,
            last_name_sort: bool = False,
            mask: bool = False,
    ):
        result_notes = self.__notes
        headers = ['Id', 'First Name', 'Last Name', 'Phone number', 'Address', 'Birth Date']
        data = []
        if first_name_filter:
            if mask:
                regex = re.compile(first_name_filter.replace('*', '.*').lower())
                regex = re.compile(first_name_filter.replace('+', '.+').lower())
                result_notes = [note for note in result_notes if regex.search(note.first_name.lower())]
            else:
                first_name_filter = first_name_filter.lower()
                result_notes = [note for note in result_notes if first_name_filter in note.first_name.lower()]
        if last_name_filter:
            headers = ['Id', 'Last Name', 'First Name', 'Phone number', 'Address', 'Birth Date']
            if mask:
                regex = re.compile(last_name_filter.replace('*', '.*').lower())
                result_notes = [note for note in result_notes if regex.search(note.last_name.lower())]
            else:
                last_name_filter = last_name_filter.lower()
                result_notes = [note for note in result_notes if last_name_filter in note.last_name.lower()]
        if phone_number_filter:
            result_notes = [note for note in result_notes if phone_number_filter in note.phone_number]
        if first_name_sort:
            result_notes.sort(key=lambda note: note.first_name)
        if last_name_sort:
            result_notes.sort(key=lambda note: note.last_name)
        for note in result_notes:
            data.append((note.id, note.first_name, note.last_name, note.phone_number, note.address, note.birth_date))
        if not data:
            print('There are no notes to show')
        print(tabulate(data, headers=headers))

