from xml.etree.ElementTree import ElementTree

from src.notebook.classes.converter import XMLConverter
from src.notebook.classes.exceptions.note_exception import NoteException
from src.notebook.classes.note import Note
from src.notebook.classes.notebook import Notebook


class UserInterface:

    def __init__(self, path='notebook.xml'):
        self.converter = XMLConverter()
        self.notebook = None
        self.path = path
        try:
            self.get_notebook_from_xml()
            print(f'There are {len(self.notebook)} note in your notebook')
        except FileNotFoundError:
            self.notebook = Notebook()
            print('New notebook created')

    def loop(self):
        while True:
            self.get_notebook_from_xml()
            ch = input('''
1. Show all notes
2. Add new note
3. Delete note
4. Edit note
5. Find note
6. Sort notes
0. Exit

Chose your option: ''')
            if not ch.isnumeric():
                print('Unexpected chose, try something else')
                continue
            match ch:
                case '1':
                    if len(self.notebook) > 0:
                        self.show_notes()
                    else:
                        print('There are no notes yet')
                case '2':
                    self.add_new_note()
                case '3':
                    self.delete_note()
                case '4':
                    self.edit_note()
                case '5':
                    self.find_note()
                case '6':
                    self.sort_notes()
                case '0':
                    print('Goodbye')
                    break
                case _:
                    print("Oops, this option is missing, you'd better try something else")
        self.write_xml_file()

    def add_new_note(self):
        first_name = input('First name(obligatory): ')
        last_name = input('Last name(obligatory): ')
        phone_number = input('Phone number(obligatory): ')
        address = input('Address: ')
        birth_date = input('Birth date: ')
        try:
            note = Note(
                first_name=first_name,
                last_name=last_name,
                phone_number=phone_number,
                address=address,
                birth_date=birth_date
            )
            self.notebook.append(note)
            print('Complete')
        except NoteException as e:
            print(e)
            print('Try once more or do something else')

    def delete_note(self):
        note_id = input('Input id of note: ')
        if note_id.isnumeric():
            data = [note for note in self.notebook if note.id == int(note_id)]
            if data:
                self.notebook.delete_note(data[0])
                print('Complete')
                return
            else:
                print('There are no notes with such id')
        else:
            print('Incorrect input')
        print('Try once more or do something else')

    def edit_note(self):
        note_id = input('Input id of note: ')
        if note_id.isnumeric():
            data = [note for note in self.notebook if note.id == int(note_id)]
            if data:
                note = data[0]
                print('Note data: ')
                print('First name: ', note.first_name)
                print('Last name: ', note.last_name)
                print('Phone number: ', note.phone_number)
                print('Address: ', note.address)
                print('Birth date: ', note.birth_date)
                edit_ch = input('''What do you want to change?
1. First name
2. Last name
3. Phone number
4. Address
5. Birth date
0. To main menu

Chose your option: ''')
                try:
                    if edit_ch.isnumeric():
                        match edit_ch:
                            case '1':
                                new_first_name = input('First name(obligatory): ')
                                note.modify_note(first_name=new_first_name)
                                print('Complete')
                            case '2':
                                new_last_name = input('Last name(obligatory): ')
                                note.modify_note(last_name=new_last_name)
                                print('Complete')
                            case '3':
                                new_phone_number = input('Phone number(obligatory): ')
                                note.modify_note(phone_number=new_phone_number)
                                print('Complete')
                            case '4':
                                new_address = input('Address: ')
                                note.modify_note(address=new_address)
                                print('Complete')
                            case '5':
                                new_birth_date = input('Birth date: ')
                                note.modify_note(birth_date=new_birth_date)
                                print('Complete')
                            case '0':
                                return
                            case '_':
                                print('Incorrect input')
                    return
                except NoteException as e:
                    print(e)
            else:
                print('There are no notes with such id')
        else:
            print('Incorrect input')
        print('Try once more or do something else')

    def find_note(self):
        find_ch = input('''
1. Find by First Name
2. Find by Last name
3. Find by Phone number
0. To main menu

Chose your option: ''')
        if find_ch.isnumeric():
            match find_ch:
                case '1':
                    first_name_filter = input('''
Input first name of note you want to find or part of it
(You can also use a mask: . - one symbol, + - at least one symbol , * - any number of symbols): ''')
                    if '.' in first_name_filter or '+' in first_name_filter or '*' in first_name_filter:
                        self.show_notes(first_name_filter=first_name_filter, mask=True)
                    else:
                        self.show_notes(first_name_filter=first_name_filter)
                case '2':
                    last_name_filter = input('''
                    Input last name of note you want to find or part of it
                    (You can also use a mask: . - one symbol, + - at least one symbol , * - any number of symbols):
                    ''')
                    if '.' in last_name_filter or '+' in last_name_filter or '*' in last_name_filter:
                        self.show_notes(last_name_filter=last_name_filter, mask=True)
                    else:
                        self.show_notes(last_name_filter=last_name_filter)
                case '3':
                    phone_number_filter = input('Input phone number of note you want to find or part of it: ')
                    self.show_notes(phone_number_filter=phone_number_filter)
                case '0':
                    return
                case '_':
                    print('Incorrect input')
                    print('Try once more or do something else')
            return
        else:
            print('Incorrect input')
        print('Try once more or do something else')

    def sort_notes(self):
        sort_ch = input('''
1. Sort by First Name
2. Sort by Last name
0. To main menu

Chose your option: ''')
        if sort_ch.isnumeric():
            match sort_ch:
                case '1':
                    self.show_notes(first_name_sort=True)
                case '2':
                    self.show_notes(last_name_sort=True)
                case '0':
                    return
                case '_':
                    print('Incorrect input')
                    print('Try once more or do something else')
            return
        else:
            print('Incorrect input')
        print('Try once more or do something else')

    def write_xml_file(self):
        converted_notebook = self.converter.convert_to_xml(self.notebook)
        tree = ElementTree(converted_notebook)
        tree.write(self.path)

    def get_notebook_from_xml(self):
        tree = ElementTree()
        tree.parse(self.path)
        convert_notebook = tree.getroot()
        self.notebook = self.converter.convert_to_obj(convert_notebook)

    def show_notes(
            self,
            first_name_filter: str = None,
            last_name_filter: str = None,
            phone_number_filter: str = None,
            first_name_sort: bool = False,
            last_name_sort: bool = False,
            mask: bool = False,
    ):
        self.notebook.show_notes(
            first_name_filter,
            last_name_filter,
            phone_number_filter,
            first_name_sort,
            last_name_sort,
            mask
        )


if __name__ == '__main__':
    ui = UserInterface()
    ui.add_new_note()
