from xml.etree.ElementTree import Element
from src.notebook.classes.note import Note
from src.notebook.classes.notebook import Notebook


class XMLConverter:

    @classmethod
    def convert_to_xml(cls, obj: Note or Notebook) -> Element:
        if type(obj) == Note:
            attrib = dict(obj.__dict__)
            attrib.pop('id')
            convert_note = Element('Note', attrib=attrib)
            return convert_note
        elif type(obj) == Notebook:
            xml_notebook = Element('Notebook')
            for note in obj:
                xml_notebook.append(cls.convert_to_xml(note))
            return xml_notebook

    @classmethod
    def convert_to_obj(cls, element: Element) -> Note or Notebook:
        if element.tag == 'Note':
            note = Note(
                first_name=element.attrib['first_name'],
                last_name=element.attrib['last_name'],
                phone_number=element.attrib['phone_number'],
                address=element.attrib['address'],
                birth_date=element.attrib['birth_date'],
                )
            return note
        elif element.tag == 'Notebook':
            notebook = Notebook()
            for child in element:
                notebook.append(cls.convert_to_obj(child))
            return notebook


if __name__ == '__main__':
    notebook = Notebook()
    notebook.append(Note('a', 'h', '+123456789123', '', ''))
    notebook.append(Note('b', 'g', '+123456789123', '', ''))
    notebook.append(Note('c', 'f', '+123456789123', '', ''))
    notebook.append(Note('d', 'e', '+123456789123', '', ''))
    notebook.append(Note('e', 'd', '+123456789123', '', ''))
    notebook.append(Note('f', 'c', '+123456789123', '', ''))
    notebook.append(Note('g', 'b', '+123456789123', '', ''))
    notebook.append(Note('h', 'a', '+123456789123', '', ''))

    c = XMLConverter()
    convert_notebook = c.convert_to_xml(notebook)
    for convert_note in convert_notebook:
        for key, value in convert_note.attrib.items():
            print(key, value)

    notebook = c.convert_to_obj(convert_notebook)
    notebook.show_notes(last_name_sort=True)
