from abc import ABC, abstractmethod


class NoteException(Exception, ABC):

    @abstractmethod
    def __str__(self):
        pass


if __name__ == '__main__':
    try:
        raise NoteException
    except NoteException as e:
        try:
            print(e)
        except TypeError:
            print('Correct')
