from src.notebook.classes.exceptions.note_exception import NoteException


class NoInputPhoneException(NoteException):

    def __str__(self):
        return "You don't input phone number"


if __name__ == '__main__':
    try:
        raise NoInputPhoneException
    except NoteException as e:
        print(e)
