from src.notebook.classes.exceptions.note_exception import NoteException


class WrongLastNameException(NoteException):

    def __str__(self):
        return 'There is a wrong last name(non digits only)'


if __name__ == '__main__':
    try:
        raise WrongLastNameException
    except NoteException as e:
        print(e)
