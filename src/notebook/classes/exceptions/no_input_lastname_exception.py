from src.notebook.classes.exceptions.note_exception import NoteException


class NoInputLastnameException(NoteException):

    def __str__(self):
        return "You don't input last name"


if __name__ == '__main__':
    try:
        raise NoInputLastnameException
    except NoteException as e:
        print(e)
