from src.notebook.classes.exceptions.note_exception import NoteException


class WrongPhoneException(NoteException):

    def __str__(self):
        return 'There is a wrong phone number'


if __name__ == '__main__':
    try:
        raise WrongPhoneException
    except NoteException as e:
        print(e)
