from src.notebook.classes.exceptions.note_exception import NoteException


class NoInputFirstnameException(NoteException):

    def __str__(self):
        return "You don't input first name"


if __name__ == '__main__':
    try:
        raise NoInputFirstnameException
    except NoteException as e:
        print(e)
