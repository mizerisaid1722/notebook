from src.notebook.classes.exceptions.note_exception import NoteException


class WrongFirstNameException(NoteException):

    def __str__(self):
        return 'There is a wrong first name(non digits only)'


if __name__ == '__main__':
    try:
        raise WrongFirstNameException
    except NoteException as e:
        print(e)
